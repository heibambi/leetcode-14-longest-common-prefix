var longestCommonPrefix = function(strs) {
    let prefix = strs[0];
    for(let i = 0 ; i<strs.length ; i++){
        while( strs[i].indexOf(prefix) !== 0){
            prefix = prefix.slice(0 , prefix.length - 1);
        }
    }

    return prefix;
}

const answer1 = longestCommonPrefix(["flower","flow","flight"]);
const answer2 = longestCommonPrefix(["dog","racecar","car"]);
const answer3 = longestCommonPrefix(["c","acc", "ccc"]);

console.log({
    answer1,
    answer2,
    answer3
})
